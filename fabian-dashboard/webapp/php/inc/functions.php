    function seconds($seconds) {
     
    		// CONVERT TO HH:MM:SS
    		$hours = floor($seconds/3600);
    		$remainder_1 = ($seconds % 3600);
    		$minutes = floor($remainder_1 / 60);
    		$seconds = ($remainder_1 % 60);
     
    		// PREP THE VALUES
    		if(strlen($hours) == 1) {
    			$hours = "0".$hours;
    		}
     
    		if(strlen($minutes) == 1) {
    			$minutes = "0".$minutes;
    		}
     
    		if(strlen($seconds) == 1) {
    			$seconds = "0".$seconds;
    		}
     
    		return $hours.":".$minutes.":".$seconds;
     
}
