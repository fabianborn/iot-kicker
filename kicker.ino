

#include <MQTT.h>
#include <Adafruit_DHT_Particle.h>
#include <HttpClient.h>
#include <application.h>
#include <string.h>



#define DHTTYPE DHT22		// DHT 22 (AM2302)
#define DHTPIN D2           // what pin DHT22 connected to
#define LSPIN1 D0           // Laser GAESTE TOR
#define LSPIN2 D1           // Laser HEIM TOR
#define REGAPIN D3
#define MAXBUFSIZE 250      // Max Buffer Size for messages

/* ****************************************************************
        Parameters
   **************************************************************** */ 
char *mqttserver = "";
String mqttuser = "smarterkicker";
String mqttpasswd = "B7YY:-9Z6*(-";
int mqttport = 1883 ;
int gameID = 0;

HttpClient http;
const String webserverIP = "website";
const int webserverPort = 8081; 

http_header_t headers[] = {
    { "Accept" , "*/*"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};

http_request_t request;
http_response_t response;


/* Parameter for current Game */
bool is_ongoing = false;
int goal_home = 0;
int goal_guest = 0;

String kickername;
String timestamp;

DHT dht(DHTPIN, DHTTYPE);
MQTT mqttclient(mqttserver, mqttport, callback);


// EEPROM addresses to save some values
const int EEPROM_GAMEID = 10;
// const int EEPROM_PRODPRICE = 20;


/* ****************************************************************
        functions 
   **************************************************************** */ 
void callback(char* topic, byte* payload, unsigned int length) {
    char p[length + 1];
    memcpy(p, payload, length);
    p[length] = NULL;

    if (!strcmp(p, "RED"))
        RGB.color(255, 0, 0);
    else if (!strcmp(p, "GREEN"))
        RGB.color(0, 255, 0);
    else if (!strcmp(p, "BLUE"))
        RGB.color(0, 0, 255);
    else
        RGB.color(255, 255, 255);
    delay(1000);
}

void sendData2Server(String command) {
    delay(200);
    // Read Environment
    float h = dht.getHumidity();
    float t = dht.getTempCelcius();
    // send data to webserver

    // MQTT, cloud push  
    mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/temperatur",String::format("%4.2f",t));
    mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/humidity",String::format("%4.2f",h));
    mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/location","temp");

    request.path = "/save_data.php?timestamp=" + getTime(true);
    request.path += "&gameid=" + gameID;
    request.path += "&kickername=" + kickername;
    
 
    if (command == "endgame"){
        request.path += "&event=endgame";
        http.get(request, response, headers);
    }
    if (command == "startgame"){
        request.path += "&event=startgame";
        http.get(request, response, headers);
    }
    // Particle push 
    if (command == "home"){
        request.path += "&event=goal";
        request.path += "&guest=" + 0;
        request.path += "&home=" + 1;
        request.path += "&temperature=" + String(t);
        request.path += "&humidity=" + String(h);
        request.path += "&noise="+ 100;
        http.get(request, response, headers);
        Particle.publish("goal", 
                "{\"eventSentUtcTime\": \"" + getTime(false) + 
                "\", \"kicker\": \"" + kickername + 
                "\", \"guest\": \"" + "0" +
                "\", \"home\": \"" + "1" +
                "\", \"environment\": {\"temperature\": \"" + String(t) + 
                "\", \"humidity\": \"" + String(h) +
                "\"}}", PRIVATE);
        mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/home","1");
        mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/guest","0");
    }
    if (command == "guest"){
        request.path += "&event=goal";
        request.path += "&guest=" + 1;
        request.path += "&home=" + 0;
        request.path += "&temperature=" + String(t);
        request.path += "&humidity=" + String(h);
        request.path += "&noise="+ 100;
        http.get(request, response, headers);
        Particle.publish("goal", 
                "{\"eventSentUtcTime\": \"" + getTime(false) + 
                "\", \"kicker\": \"" + kickername + 
                "\", \"guest\": \"" + "q" +
                "\", \"home\": \"" + "0" +
                "\", \"environment\": {\"temperature\": \"" + String(t) + 
                "\", \"humidity\": \"" + String(h) +
                "\"}}", PRIVATE);
        mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/guest","1");
        mqttclient.publish("/SmarterKicker/" + kickername +  "/" + gameID + "/home","0");
    }    
   
   
}

String getTime(bool format) {
    
    String tmp;
    timestamp = "";
    timestamp += String(Time.year());
    timestamp += "-";
    tmp = String(Time.month());
    if(tmp.length() < 2) {  timestamp += "0" + tmp; } else { timestamp += tmp; }
    timestamp += "-";
    tmp = String(Time.day());
    if(tmp.length() < 2) { timestamp += "0" + tmp; } else { timestamp += tmp; }
    if(format) { timestamp += "%20"; } else { timestamp += " "; }
    tmp = String(Time.hour());
    if(tmp.length() < 2) { timestamp += "0" + tmp; } else { timestamp += tmp; }    
    timestamp += ":";
    tmp = String(Time.minute());
    if(tmp.length() < 2) { timestamp += "0" + tmp;  } else { timestamp += tmp; }
    timestamp += ":";
    tmp = String(Time.second());
    if(tmp.length() < 2) { timestamp += "0" + tmp; } else { timestamp += tmp; }
    
    return timestamp;
}

const char * getEnvData()
{
    	float h = dht.getHumidity();
// Read temperature as Celsius
    	float t = dht.getTempCelcius();
// Read temperature as Farenheit
    	float f = dht.getTempFarenheit();
      	float hi = dht.getHeatIndex();
    	float dp = dht.getDewPoint();
    	float k = dht.getTempKelvin();
        
        char *x = (char*)malloc(MAXBUFSIZE * sizeof(char));
        sprintf(x,"%4.2f;%4.2f",t,h);
        
        return x;
}

void handler(const char *topic, const char *data) {
    Serial.println("received " + String(topic) + ": " + String(data));
    kickername = String(data);
}


void setup() {
    // Set PIN Mode 
    pinMode(LSPIN1, INPUT);
    pinMode(LSPIN2, INPUT);
    pinMode(REGAPIN, INPUT_PULLUP);
    
    Time.zone(+1);
    
    request.hostname = webserverIP;
    request.port = webserverPort;
    
    
    // Reading Device Name
    Particle.subscribe("particle/device/name", handler);
    Particle.publish("particle/device/name"); 
    
	Particle.publish("Status", "SmarterKicker " + kickername + " is starting...");
    
    // Enable Temperature Sensore
	dht.begin();
	// Connect MQTT Client
	mqttclient.connect(kickername + "_" + String(Time.now()), mqttuser, mqttpasswd);
	
	
	if (mqttclient.isConnected()) {
        Particle.publish(kickername, "MQTT: Connected");
        // subscripe to MQTT inbound messages
        mqttclient.subscribe("/SmarterKicker/" + kickername + "/" + gameID +  "/noise");
        mqttclient.subscribe("/SmarterKicker/" + kickername + "/" + gameID +  "/temperatur");
        mqttclient.subscribe("/SmarterKicker/" + kickername + "/" + gameID +  "/humidity");
        mqttclient.subscribe("/SmarterKicker/" + kickername + "/" + gameID +  "/location"); 
        mqttclient.subscribe("/SmarterKicker/" + kickername + "/" + gameID +  "/guest");
        mqttclient.subscribe("/SmarterKicker/" + kickername + "/" + gameID + "/home");
    }else {
        Particle.publish(kickername, "MQTT: !! NOT !! Connected");
    }

    // READ current GAME ID from EEPROM if not availabe set it to default 0
    int tmpGameID;
    EEPROM.get(EEPROM_GAMEID, tmpGameID);
    if(tmpGameID != -1) {
      // EEPROM wasn't empty 
      gameID = tmpGameID;
    }
    // Push Variable value to dashboard @ article.io
    Particle.variable("mqttserver", mqttserver);
    Particle.variable("mqttport", mqttport);
    Particle.variable("mqttuser", mqttuser);
    Particle.variable("gameID", gameID);
    
    Particle.publish("Status", String::format("Load last GameID: %d",gameID));
    delay(500);
    Particle.publish("Status", "...finished");
    
}

void loop() {
        if (mqttclient.isConnected())
            mqttclient.loop();
        // HOME
        if ((digitalRead(LSPIN1) == HIGH)){
            if (mqttclient.isConnected()) {
                goal_home=goal_home + 1;
        	    sendData2Server("home");
            }
            delay(15000);
        }
        // GUEST 
        if ((digitalRead(LSPIN2) == HIGH)){
            if (mqttclient.isConnected()) {
                sendData2Server("guest");
                goal_guest=goal_guest + 1;
            }
            delay(15000);
        }

        if ((digitalRead(REGAPIN) == LOW)){
            Particle.publish(kickername, String::format("%s : RESTART_GAME" , String(getTime(false))) , 60, PRIVATE);
            gameID=gameID + 1;
            goal_guest = 0;
            goal_home = 0;
            delay(200);
            is_ongoing = true;
           // EEPROM.put(EEPROM_GAMEID, gameID);
            delay(300);
            sendData2Server("startgame");
        }

    delay(300);
}
